# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

A demo of how Spring boot can be used 

1. with Mongodb 
2. with basic authentication/authorization setup
3. with provision for sending messages to a JMS queue.
4. with custom filters that keeps count of no of hits in memory for a URI and prints the
   execution time as well for a controller method.
   

### How do I get set up? ###

1.  clone the repo
2.  generate eclipse class path : gradlew clEcl ecl assemble
3.  run the tests : gradlew clean build
4.  Import the project to eclipse
5.  You need to have mongodb and active-mq installed and running.
6.  If active-mq and Mongo are up and running the server would boot up properly without any error messages.
7. You can run the application either from the command-line or eclipse as detailed below.


### running from eclipse ###
Open the Main.java class and right click and select option : Run as Java application.


### running from Command line ###
In the command line, go to the project. Run ./gradlew bootRun for *nix systems and
gradlew bootRun for windows.


### In memory JMS ###

If you just need a message broker up and running then there is also an option to use
the in-memory JMS. This will be started by default when spring boot server starts up.
In order to enable this : comment out the spring.activemq.broker-url key/value paur
from the application.properties.


### How to test the application ? ###
1.  Open a rest client like Advanced Rest Client (installed in Chrome browser)
2.  You need to set 2 headers with values namely :
    9.1 : content-type : application/hal+json
    9.2 : accept : application/hal+json
3. Try to do a GET on the URL : http://localhost:8080/persons
4. It will ask for authentication. Enter user/password as user/user.
5. This would fail with 403 Forbidden error.
6. Login again as admin/admin.
7. This time it should return a 200 OK status.
8. Do a POST to http://localhost:8080/persons with a body like :

    {
	   name : "foo",
	   dob : "01-01-1970",
	   address: "222B, Baker Street"
	}

9. A person would be created with a return status of 200 OK. You can also check in the 
    mongo db to see if the person was created.
10. You can modify a person object by doing a PUT in the self-link of the person created.
11. In order to send a message to a JMS queue, you need to first create a queue with name "sampleQueue". (check in active-mq documentation)
    * no need to create queue explicitly if you are running in-memory message broker.

12. Once the queue has been setup, you can do a POST onto : http://localhost:8080/persons/send with a body similiar to the POST above.
13. If this is successful you can see 2 messages in the console : 
    User <user> sent person <name>
	Rcvd <name>
14. If you are running activeMq externally it is possible to see the messages that were
    enqueued/dequeued from the web-console. 
15. Similiarly you can try other URLs for DELETE : http://localhost:8080/persons/<id>(for deleting a person)