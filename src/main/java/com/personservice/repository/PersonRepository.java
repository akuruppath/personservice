package com.personservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.personservice.core.Person;

/**
 * Repository class for Person. THe boiler plate CRUD methods are defined in the
 * MongoRepository interface.
 * 
 * @author ajay
 *
 */
public interface PersonRepository extends MongoRepository<Person, String> {

}
