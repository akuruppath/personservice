package com.personservice.common;

public final class Constants {

	private Constants() {

	}

	public static final String SLASH = "/";
	public static final String PERSONS = "persons";
	public static final String PERSON_ID = "person-id";
	public static final String SEND = "send";

	public static final String PERSON_ID_TEMPLATE = "{" + PERSON_ID + "}";

	public static final String SEND_URL = SLASH + SEND;
	public static final String PERSONS_URL = SLASH + PERSONS;
	public static final String PERSON_URL = PERSONS_URL + SLASH + PERSON_ID_TEMPLATE;
	public static final String PERSONS_SEND_URL = PERSONS_URL + SEND_URL;

	public static final String APPLICATION_HAL_JSON = "application/hal+json";
}
