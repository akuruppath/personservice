package com.personservice.common;

/**
 * Constants used for href.
 * @author ajay
 *
 */
public final class RestRelationConstants {

	private RestRelationConstants() {

	}

	public static final String CREATE = "create";
	public static final String UPDATE = "update";
	public static final String DELETE = "delete";

}
