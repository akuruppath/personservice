package com.personservice.jms.receiver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.personservice.core.Person;
import com.personservice.repository.PersonRepository;

/**
 * Class for retrieving a message from a JMS queue.
 * 
 * @author ajay
 *
 */
@Component
public class PersonReceiver {

	private PersonRepository personRepository;

	@Autowired
	public PersonReceiver(PersonRepository personRepository) {
		this.personRepository = personRepository;
	}

	@JmsListener(destination = "sampleQueue")
	public void receivePerson(Person person) {
		System.out.println("Rcvd  <" + person.getName() + ">");
		personRepository.save(person);
	}
}
