package com.personservice.resourceprocessor.collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Component;

import com.personservice.core.Person;
import com.personservice.resourceprocessor.PersonResourceProcessor;

/**
 * Class for converting a collection of domain objects into a collection of REST
 * resources.
 * 
 * @author ajay
 *
 */
@Component
public class PersonsResourceProcessor implements ResourceProcessor<Resources<Resource<Person>>> {

	private PersonResourceProcessor personResourceProcessor;

	@Autowired
	public PersonsResourceProcessor(PersonResourceProcessor personResourceProcessor) {
		this.personResourceProcessor = personResourceProcessor;
	}

	@Override
	public Resources<Resource<Person>> process(Resources<Resource<Person>> personResources) {
		personResources.forEach(resource -> {
			personResourceProcessor.process(resource);
		});
		return personResources;
	}
}
