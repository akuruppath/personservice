package com.personservice.resourceprocessor;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static com.personservice.common.RestRelationConstants.CREATE;
import static com.personservice.common.RestRelationConstants.DELETE;
import static com.personservice.common.RestRelationConstants.UPDATE;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

import com.personservice.controller.PersonController;
import com.personservice.core.Person;

/**
 * Class for converting domain object into a REST resource. All the links for
 * HATEOAS are added here to the resource.
 * 
 * @author ajay
 *
 */
@Component
public class PersonResourceProcessor implements ResourceProcessor<Resource<Person>> {

	@Override
	public Resource<Person> process(Resource<Person> personResource) {
		Person person = personResource.getContent();
		personResource.add(linkTo(methodOn(PersonController.class).getPerson(person.getId())).withSelfRel());
		personResource.add(linkTo(methodOn(PersonController.class).getAllPersons()).withRel(CREATE));
		personResource.add(linkTo(methodOn(PersonController.class).getPerson(person.getId())).withRel(UPDATE));
		personResource.add(linkTo(methodOn(PersonController.class).getPerson(person.getId())).withRel(DELETE));
		return personResource;
	}

}
