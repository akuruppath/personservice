package com.personservice.filters;

import java.io.IOException;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.google.common.collect.Maps;

/**
 * Just for learning/demo : Keeps the number of URI hits in memory.
 * 
 * @author ajay
 *
 */
@Component
public class CounterFilter extends OncePerRequestFilter {

	private static final Map<String, Integer> COUNTER_MAP = Maps.newConcurrentMap();

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		keepCount(request);
		filterChain.doFilter(request, response);
	}

	private void keepCount(HttpServletRequest request) {
		String path = request.getRequestURI();
		Integer existingCount = COUNTER_MAP.get(path);
		COUNTER_MAP.put(path, existingCount == null ? 0 : existingCount + 1);
		System.out.println(COUNTER_MAP);
	}

}
