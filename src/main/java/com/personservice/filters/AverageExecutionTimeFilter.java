package com.personservice.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * Just for learning/demo : Prints the execution time for a controller method.
 * 
 * @author ajay
 *
 */
@Component
public class AverageExecutionTimeFilter extends OncePerRequestFilter {

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		long start = System.currentTimeMillis();
		try {
			filterChain.doFilter(request, response);
		} finally {
			long end = System.currentTimeMillis();
			System.out.println("Time taken to execute the endPoint of [" + request.getRequestURL() + "] is "
					+ (end - start) + "ms.");
		}

	}

}
