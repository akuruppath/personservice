package com.personservice.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Configuration class for setting up the basic authentication. The in-memory
 * users are specified here.
 * 
 * @author ajay
 *
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private static final String PERSONS_URL = "/persons/**";
	private static final String ADMIN = "admin";
	private static final String USER = "user";
	private static final String ROLE_USER = "USER";
	private static final String ROLE_ADMIN = "ADMIN";

	private static final String[] USER_ROLES = { ROLE_USER };
	private static final String[] ADMIN_ROLES = { ROLE_USER, ROLE_ADMIN };

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder builder) throws Exception {
		builder.inMemoryAuthentication().withUser(USER).password(USER).roles(USER_ROLES).and().withUser(ADMIN)
				.password(ADMIN).roles(ADMIN_ROLES);
	}

	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers(PERSONS_URL).hasAnyRole(ROLE_USER, ROLE_ADMIN).and().httpBasic().and()
				.csrf().disable();
	}

}
