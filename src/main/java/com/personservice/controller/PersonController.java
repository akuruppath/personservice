package com.personservice.controller;

import static com.personservice.common.Constants.APPLICATION_HAL_JSON;

import static com.personservice.common.Constants.PERSONS_URL;
import static com.personservice.common.Constants.PERSON_ID;
import static com.personservice.common.Constants.PERSON_URL;
import static com.personservice.common.Constants.PERSONS_SEND_URL;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.personservice.core.Person;
import com.personservice.repository.PersonRepository;
import com.personservice.resourceprocessor.PersonResourceProcessor;
import com.personservice.resourceprocessor.collection.PersonsResourceProcessor;

/**
 * Controller class for Person. Contains methods for CRUD operations on Person.
 * 
 * @author ajay
 *
 */
@RestController
public class PersonController {

	private static final String QUEUE_NAME = "sampleQueue";

	private PersonRepository personRepository;
	private PersonResourceProcessor personResourceProcessor;
	private PersonsResourceProcessor personsResourceProcessor;
	private JmsTemplate jmsTemplate;

	@Autowired
	public PersonController(PersonRepository personRepository, PersonResourceProcessor personResourceProcessor,
			PersonsResourceProcessor personsResourceProcessor, JmsTemplate jmsTemplate) {
		this.personRepository = personRepository;
		this.personResourceProcessor = personResourceProcessor;
		this.personsResourceProcessor = personsResourceProcessor;
		this.jmsTemplate = jmsTemplate;
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(method = RequestMethod.POST, path = PERSONS_URL, consumes = APPLICATION_HAL_JSON, produces = APPLICATION_HAL_JSON)
	public ResponseEntity<Resource<Person>> createPerson(@Valid @RequestBody Person person) {
		Person savedPerson = personRepository.save(person);
		return new ResponseEntity<>(personResourceProcessor.process(new Resource<Person>(savedPerson)),
				getHeaders(savedPerson.getId()), HttpStatus.CREATED);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(method = RequestMethod.GET, path = PERSONS_URL, produces = APPLICATION_HAL_JSON)
	public ResponseEntity<Resources<Resource<Person>>> getAllPersons() {
		Resources<Resource<Person>> persons = Resources.wrap(personRepository.findAll());
		return new ResponseEntity<>(personsResourceProcessor.process(persons), HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping(method = RequestMethod.GET, path = PERSON_URL, consumes = APPLICATION_HAL_JSON, produces = APPLICATION_HAL_JSON)
	public ResponseEntity<Resource<Person>> getPerson(@PathVariable(value = PERSON_ID) final String personId) {
		Person person = getIfPersonExists(personId);
		return new ResponseEntity<>(personResourceProcessor.process(new Resource<Person>(person)), HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(method = RequestMethod.PUT, path = PERSON_URL, consumes = APPLICATION_HAL_JSON, produces = APPLICATION_HAL_JSON)
	public ResponseEntity<Resource<Person>> updatePerson(@Valid @RequestBody Person updatedPerson,
			@PathVariable(value = PERSON_ID) final String personId) {
		Person savedPerson = getIfPersonExists(personId);
		updatedPerson.setId(savedPerson.getId());
		updatedPerson = personRepository.save(updatedPerson);
		return new ResponseEntity<>(personResourceProcessor.process(new Resource<Person>(updatedPerson)),
				HttpStatus.OK);
	}

	@PreAuthorize("has_role('ROLE_ADMIN')")
	@RequestMapping(method = RequestMethod.DELETE, path = PERSON_URL, consumes = APPLICATION_HAL_JSON, produces = APPLICATION_HAL_JSON)
	public ResponseEntity<Resource<Person>> deletePerson(@PathVariable(value = PERSON_ID) final String personId) {
		Person person = getIfPersonExists(personId);
		personRepository.delete(person);
		return new ResponseEntity<>(personResourceProcessor.process(new Resource<Person>(person)),
				HttpStatus.NO_CONTENT);
	}

	@RequestMapping(method = RequestMethod.POST, path = PERSONS_SEND_URL, consumes = APPLICATION_HAL_JSON)
	public void sendPerson(@Valid @RequestBody Person person, HttpServletRequest request) {
		System.out.println("User <" + request.getUserPrincipal().getName() + "> sent <" + person.getName() + ">");
		jmsTemplate.convertAndSend(QUEUE_NAME, person);
	}

	private Person getIfPersonExists(final String personId) {
		Person person = personRepository.findOne(personId);
		Assert.notNull(person, "Person with id [" + personId + "] does not exist");
		return person;
	}

	private HttpHeaders getHeaders(String id) {
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(id).toUri());
		return headers;
	}
}
