package test.com.personservice.integration;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.google.gson.Gson;
import com.personservice.common.Constants;
import com.personservice.core.Person;
import com.personservice.main.Main;

import test.com.personservice.integration.configuration.MongoIntegrationTestConfiguration;

/**
 * Integration test for create/get methods with roles specified.
 * 
 * @author ajay
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = Main.class)
@ContextConfiguration(classes = MongoIntegrationTestConfiguration.class)
@AutoConfigureMockMvc
public class PersonControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	@WithMockUser(username = "user", roles = "USER")
	public void testCreatePersonAsUser() throws Exception {
		Person person = new Person();
		person.setName("Sherlock");
		person.setAddress("222B, Baker Street");
		person.setDob("01-01-1970");

		this.mockMvc
				.perform(
						post("/persons").contentType(Constants.APPLICATION_HAL_JSON).content(new Gson().toJson(person)))
				.andExpect(status().isForbidden());
	}

	@Test
	@WithMockUser(username = "admin", roles = "ADMIN")
	public void testCreatePersonAsAdmin() throws Exception {
		this.mockMvc.perform(get("/persons")).andExpect(status().isOk());

		Person person = new Person();
		person.setName("Sherlock");
		person.setAddress("222B, Baker Street");
		person.setAddress("01-01-1970");

		this.mockMvc
				.perform(
						post("/persons").contentType(Constants.APPLICATION_HAL_JSON).content(new Gson().toJson(person)))
				.andExpect(status().isCreated());
	}

	@Test
	@WithMockUser(username = "user", roles = "USER")
	public void testGetAllPersonsAsUser() throws Exception {
		this.mockMvc.perform(get("/persons")).andExpect(status().isForbidden());
	}

	@Test
	@WithMockUser(username = "admin", roles = "ADMIN")
	public void testGetAllPersonsAsAdmin() throws Exception {
		this.mockMvc.perform(get("/persons")).andExpect(jsonPath("$._embedded.personList[0].name", is("Sherlock")))
				.andExpect(status().isOk());
	}

}
